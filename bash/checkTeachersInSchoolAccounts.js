/**
 * This script check if there is id in SchoolAccount teachers. If there isn't
 * It will add the id field
 * you should run it twice
 */

// register mongoose models
require("../models");
const ObjectId = require("mongodb").ObjectID;
const SchoolAccount = require("../models/SchoolAccount");
const User = require("../models/User");
const PRODUCTION_MONGODB_URI =
  "mongodb://efratgur:kehilamenagenet@139.59.209.250:27017/anthology";
const DEVELOPMENT_MONGODB_URI = "mongodb://localhost:27017/anthology";
let ids = [];

// connect to mongo
require("../hendrix/mongoose")(DEVELOPMENT_MONGODB_URI, async () => {
  const docs = await SchoolAccount.find({});
  for (const acc of docs) {
    for (let [index, teacher] of acc.teachers.entries()) {
      let uFlag = false;
      if (!teacher.id) {
        // get the teacher
        const user = await User.findOne({ name: teacher.name }).lean().exec();
        console.log(user.name, acc.schoolName);

        // remove and add it
        acc.teachers.splice(index, 1);
        acc.teachers.push({ name: user.name, id: user._id.toString() });
        uFlag = true;
      }
      if (uFlag) {
        const docs = await SchoolAccount.updateOne(
          {
            baseAccessCode: acc.baseAccessCode,
          },
          acc
        );
        console.log(docs);
      }
    }
  }
  process.exit(0);
});
