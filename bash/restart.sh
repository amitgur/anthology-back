#!/bin/bash

# restart with git and forever

echo 'starting node server with forever'

    echo "Stoping node thread"
    forever stop anthologyServer.js

    echo "Restarting Node Server"
    # remove from dir

    rm /home/bandpad/bandpad/anthology-back/log/forever.log

    forever start -l /home/bandpad/bandpad/anthology-back/log/forever.log anthologyServer.js
