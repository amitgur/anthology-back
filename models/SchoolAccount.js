const mongoose = require("mongoose");

const schoolAccountSchema = new mongoose.Schema({
  createDate: String,
  baseAccessCode: {
    type: String,
    unique: true,
  },
  studentsNumber: String,
  teachers: { type: Array, default: [] },
  lastIndex: { type: Number, default: 1 },
  schoolName: String,
  schoolId: String,
  paid: { type: Boolean, default: false },
  yearPass: { type: Array, default: null },
  details: String,
});

const SchoolAccount = mongoose.model("SchoolAccount", schoolAccountSchema);

module.exports = SchoolAccount;
