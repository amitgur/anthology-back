/*jslint node: true */ "use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
/**
 * UserData Schema
 */
const UserDataSchema = new mongoose.Schema({
  userId: {
    type: String,
    unique: true,
  },
  baseAccessCode: String,
  name: String,

  compositions: { type: Schema.Types.Mixed, default: {} },
  visits: { type: Schema.Types.Mixed, default: {} },
});

const UserData = mongoose.model("UserData", UserDataSchema);
module.exports = UserData;
