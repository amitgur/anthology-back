const mongoose = require("mongoose");

const notPaidSchema = new mongoose.Schema({
  baseAccessCode: {
    type: String,
    unique: true,
  },
});

const NotPaid = mongoose.model("NotPaid", notPaidSchema);

module.exports = NotPaid;
