/**
 * Connect to MongoDB.
 */
const mongoose = require("mongoose");

module.exports = function (uri, cb) {
  mongoose.set("useFindAndModify", false);
  mongoose.set("useCreateIndex", true);
  mongoose.set("useNewUrlParser", true);
  mongoose.set("useUnifiedTopology", true);

  mongoose.connect(uri);
  console.log(`trying connection to ${uri}`);

  mongoose.connection.on("error", (err) => {
    console.error(err);
    console.log(
      `${chalk.yellow(
        "✗"
      )} MongoDB connection error. Please make sure MongoDB is running.`
    );
    process.exit();
  });

  mongoose.connection.on("connected", function () {
    console.log(`Mongoose connected to ${uri}`);
    if (cb) {
      cb();
    }
  });
};
