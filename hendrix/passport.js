/*jslint node: true */ "use strict";

const mongoose = require("mongoose"),
  LocalStrategy = require("passport-local").Strategy;
const logger = require("./logger").logger;

module.exports = function (passport) {
  console.log("Loading user environment for passport");

  const Users = mongoose.model("User");

  // Serialize sessions
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    Users.findOne(
      {
        _id: id,
      },
      "-salt -hashed_password",
      function (err, user) {
        done(err, user);
      }
    );
  });

  // Use local strategy
  passport.use(
    new LocalStrategy(function (username, password, done) {
      Users.findOne(
        {
          username: username,
        },
        function (err, user) {
          if (err) {
            logger.error(
              `some error in login, username: ${username}, password: ${password}`
            );
            logger.error(err);
            return done(null, false, {
              message: "תקלה בכניסת משתמש",
            });
          }
          if (!user) {
            logger.error(
              `username or access code not found, username: ${username}`
            );
            return done(null, false, {
              message: "קוד כניסה או מייל לא נמצאו",
            });
          }
          if (!user.authenticate(password)) {
            logger.error(
              `wrong password, username: ${username}, password: ${password}`
            );
            return done(null, false, {
              message: "הסיסמא אינה נכונה",
            });
          }

          return done(null, user);
        }
      );
    })
  );
};
