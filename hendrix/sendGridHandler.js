const sgMail = require("@sendgrid/mail"),
  sgClient = require("@sendgrid/client");

let lists = null;

exports.initSendGrid = function () {
  // init sendgrid
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  sgClient.setApiKey(process.env.SENDGRID_API_KEY);

  // get contact lists
  const requestObj = {
    method: "GET",
    url: "/v3/contactdb/lists",
  };

  // init contact lists
  sgClient.request(requestObj).then(
    ([response, body]) => {
      console.log(`return ${response.statusCode} from sendgird list request`);

      lists = response.body.lists;

      // test line exports.addToList("TeachersCH", "testing@hotmail.com");
    },
    (error) => {
      console.log("error on add receipt");
      console.log(error.message, "error");
    }
  );
};

exports.sendBandPadMail = function (subject, text, to) {
  let email = {
    to: to,
    from: "kmenagenet@gmail.com",
    subject: subject,
    html: text,
  };

  return new Promise(function (resolve, reject) {
    sgMail.send(email, function (err, json) {
      if (err) {
        console.error("Error in BandPadMail");
        console.error(err);
        console.error(json);
        return reject(err);
      } else {
        return resolve();
      }
    });
  });
};

exports.sendMail = function (subject, text, to) {
  let email = {
    to: to,
    from: "kmenagenet@gmail.com",
    subject: subject,
    html: text,
  };

  return new Promise(function (resolve, reject) {
    sgMail.send(email, function (err, json) {
      if (err) {
        console.log("Error in BandPadMail");
        console.log(err);
        console.log(json);
        return reject(err);
      } else {
        return resolve();
      }
    });
  });
};

exports.addToList = function (list, email) {
  // first find that list

  return new Promise((resolve, reject) => {
    let listId = lists.filter((l) => l.name === list);

    if (listId.length !== 1) {
      reject(`Cannot locate list id for list`);
    } else {
      listId = listId[0].id;
    }

    let reqObj = {
      method: "POST",
      url: "/v3/contactdb/recipients",
      body: [{ email }],
    };

    // first add the recipient
    sgClient
      .request(reqObj)
      .then(([response, body]) => {
        if (body.new_count === 0) {
          console.log(`email ${email} already exist`);
          return resolve();
        }
        if (body.new_count !== 1) {
          return reject(
            `error trying to add email, new_count is ${body.new_count}  ${email} to list ${list}`
          );
        }
        const recipientId = body.persisted_recipients[0];

        // second, add to legacy marketing list
        reqObj = {
          method: "POST",
          url: `/v3/contactdb/lists/${listId}/recipients/${recipientId}`,
        };

        sgClient
          .request(reqObj)
          .then(([response]) => {
            if (response.statusCode !== 201) {
              return reject(
                `got code ${response.statusCode} trying to add email ${email} to list ${list}`
              );
            } else {
              return resolve();
            }
          })
          .catch((err) => {
            console.log(err);
            return reject(`error trying to add email ${email} to list ${list}`);
          });
      })
      .catch((err) => {
        console.log(err);
        reject(`error trying to add email ${email} as recipient`);
      });
  });
};
