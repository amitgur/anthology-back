/**
 * Module dependencies.
 */
const express = require("express");
const chalk = require("chalk");
const passport = require("passport");
const mongoose = require("mongoose");

describe("tests", () => {
  beforeAll(async (done) => {
    // connect to mongo
    require("../hendrix/mongoose")(
      "mongodb://localhost:27017/anthology-test",
      () => {
        // register mongoose models
        require("../models");

        // create passport
        const passportConfig = require("../hendrix/passport")(passport);

        const app = express();

        done();
      }
    );
  });

  afterAll(async (done) => {
    mongoose.connection.close();
  });

  it("adds 1 + 2 to equal 3", () => {
    expect(1 + 2).toBe(3);
  });
});
