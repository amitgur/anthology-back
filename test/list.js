/**
 * Download a docx file located on the google drive. then you can change it to html using
 * pandoc file.docx -f docx -t html -o index.html
 * @type {string}
 * https://www.googleapis.com/drive/v2/files?key=[YOUR_API_KEY]
 */

const fileId = "1kNwCQ-Kahgx2bw62QbdLh_4405yhTfwC";
const apiKey = "AIzaSyBxiSUkCzfc8iaX-cMFOo4HkVzRbfGJ2Wo";
const { google } = require("googleapis");
const axios = require("axios");
const fs = require("fs");
const url = `https://www.googleapis.com/drive/v2/files?key=${apiKey}`;

async function downloadDoc() {
  const response = await axios({
    url,
    method: "GET",
    responseType: "stream",
  });

  return new Promise((resolve, reject) => {
    console.log(resolve.data);
    resolve();
  });
}

downloadDoc();
