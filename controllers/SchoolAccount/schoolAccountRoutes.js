const schoolAccountCtrl = require("./schoolAccountCtrl");

module.exports = function (app) {
  app.post("/create_school_account", schoolAccountCtrl.createSchoolAccount);
  app.post("/edit_school_account", schoolAccountCtrl.editSchoolAccount);
  app.get("/get_school_accounts", schoolAccountCtrl.getSchoolAccounts);
  app.get("/get_school_account", schoolAccountCtrl.getSchoolAccount);
};
