const SchoolAccount = require("../../models/SchoolAccount");
const accessCodeCtrl = require("../User/accessCodeCtrl");

exports.createSchoolAccount = async function (req, res, next) {
  try {
    req.body.baseAccessCode = await accessCodeCtrl.createBaseAccessCode(10);
    const schoolAccount = new SchoolAccount(req.body);
    let newSchoolAccount = await schoolAccount.save();
    return res.sendStatus(200);
  } catch (e) {
    return next(e);
  }
};

exports.editSchoolAccount = async function (req, res, next) {
  let doc;
  try {
    doc = await SchoolAccount.findOneAndUpdate(
      { baseAccessCode: req.body.baseAccessCode },
      req.body.schoolAccount
    );
    return res.sendStatus(200);
  } catch (e) {
    return next(e);
  }
};

// get all the cities for step 1
exports.getSchoolAccounts = async function (req, res, next) {
  try {
    const schoolAccounts = await SchoolAccount.find({}).exec();
    return res.json(schoolAccounts);
  } catch (e) {
    return next(e);
  }
};

// get school account
exports.getSchoolAccount = async function (req, res, next) {
  try {
    const schoolAccount = await SchoolAccount.findOne({
      baseAccessCode: req.query.baseAccessCode,
    }).exec();
    return res.json(schoolAccount);
  } catch (e) {
    return next(e);
  }
};
