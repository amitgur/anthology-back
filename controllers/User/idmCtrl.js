const axios = require("axios");
const logger = require("../../hendrix/logger").logger;

exports.idmAuthGetUser = async function (req, res) {
  let response;

  // get access token
  try {
    response = await axios.post(
      "https://is.remote.education.gov.il/nidp/oauth/nam/token",
      null,
      {
        params: {
          grant_type: "authorization_code",
          client_id: "6642f919-e342-4ca8-bacc-d6e4444ad991",
          client_secret:
            "ugK0XEI6RwWzyLgt3LWG-2nqvZjQdfUQ4gkhim6WOE1ipC5g23j8t7yuHprrFIi8DCzsmldNQE_4aX_uYySGHg",
          code: req.query.code,
          redirect_uri: "https://anthology.muzipad.co/apiV1/auth_user_idm",
          resourceServer: "EDU",
        },
      }
    );
    const headers = {
      Authorization: "Bearer " + response.data.access_token,
    };

    // get user info
    response = await axios.post(
      "https://is.remote.education.gov.il/nidp/oauth/nam/userinfo",
      null,
      { headers: headers }
    );
    logger.info(
      `got user from IDM, name:  ${
        response.data.given_name + " " + response.data.family_name
      }`
    );

    // login user width response.data

    res.redirect("/#/menu");
  } catch (err) {
    res.redirect("/#/idmError");
  }
};
