const logger = require("../../hendrix/logger").logger;
const SchoolAccount = require("../../models/SchoolAccount");

// create random token
function randomToken() {
  let result = "";
  const characters = "123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < 4; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

// create access code and check it
async function cb() {
  let token = randomToken().toString();
  try {
    const newSchoolAccount = await SchoolAccount.findOne({
      baseAccessCode: token,
    }).lean();
    if (!newSchoolAccount) {
      // did not find that token
      return token;
    } else {
      return null;
    }
  } catch (e) {
    throw e;
  }
}

// promise for create base access code
exports.createBaseAccessCode = async function (tries) {
  try {
    while (tries > 0) {
      logger.info(`trying to create base code, remaining ${tries}`);
      tries--;

      let token = await cb();
      if (token) {
        logger.info(`token ${token} was accepted`);
        return token;
      } else {
        logger.info(`token ${token} was found, remaining ${tries}`);
      }
    }
    throw new Error("didn't found token after 10 retires");
  } catch (e) {
    throw e;
  }
};

exports.createStudentAccessCode = function (baseAccessCode, lastIndex) {
  let accessCode = lastIndex.toString();
  const to = 3 - accessCode.length;

  for (let i = 0; i < to; i++) {
    accessCode = `0${accessCode}`;
  }

  return `${baseAccessCode}-${accessCode}`;
};
