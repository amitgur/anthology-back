const userCtrl = require("./userCtrl");
const studentsCtrl = require("./studentsCtrl");
const passport = require("passport");
const SchoolAccount = require("../../models/SchoolAccount");
const AnalyticsCtrl = require("./analyticsCtrl");
const IDMCtrl = require("./idmCtrl");
const rateLimit = require("express-rate-limit");

const apiLimiter = rateLimit({
  windowMs: 3 * 60 * 1000, // 3 minutes
  max: 10, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
  standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
  legacyHeaders: false, // Disable the `X-RateLimit-*` headers
});

// sign in
module.exports = function (app) {
  app.post(
    "/sign_in",
    userCtrl.verifyCapacha,
    apiLimiter,
    passport.authenticate("local", { failureFlash: true }),
    userCtrl.getUser
  );

  app.post("/sign_up", userCtrl.verifyCapacha, userCtrl.signUp);

  app.post("/profile_edit", userCtrl.profileEdit);

  app.post("/forgot_password", userCtrl.forgotPassword);

  app.post("/student_sign_up", userCtrl.studentSignUp);

  app.post("/sign_out", userCtrl.signOut);

  app.get("/get_user", userCtrl.getUser);

  app.get("/get_teachers", userCtrl.getTeachers);

  app.get("/get_all_teacher_fields", userCtrl.getAllTeacherFields);

  // get authentication message
  app.get("/get_message", userCtrl.getMessage);

  // students and student data for teacher
  app.get("/get_students", studentsCtrl.getStudents);

  app.delete("/delete_student", studentsCtrl.deleteStudent);

  // update user visit by year { month { day
  app.post("/update_user_visits", AnalyticsCtrl.postUserVisits);

  /**
   * IDM
   */
  app.get("/auth_user_idm", IDMCtrl.idmAuthGetUser);
};
