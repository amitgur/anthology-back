const User = require("../../models/User");
const analyticsCtrl = require("../User/analyticsCtrl");
const SchoolAccount = require("../../models/SchoolAccount");
const logger = require("../../hendrix/logger").logger;

// get students and students data for teacher
exports.getStudents = async function (req, res, next) {
  if (!req.user) {
    return next(new Error("Teacher is not logged in"));
  }

  if (req.user.profile !== "teacher" && req.user.profile !== "admin") {
    return next(new Error("User is not a teacher"));
  }

  let schoolAccounts, schoolStudents;

  // get all access code for the teacher
  try {
    schoolAccounts = await SchoolAccount.find({
      teachers: { $elemMatch: { id: req.user.id } },
    }).lean();
  } catch (e) {
    return next(new Error(e));
  }

  let students = [],
    schools = [];

  // get all student for all access codes and fetch data
  for (const schoolAccount of schoolAccounts) {
    try {
      schoolStudents = await User.find({
        baseAccessCode: schoolAccount.baseAccessCode,
      })
        .lean()
        .exec();
    } catch (e) {
      return next(new Error(e));
    }

    // add school name to students
    schoolStudents = schoolStudents.map((s) => {
      return {
        id: s._id,
        name: s.name,
        accessCode: s.username,
        weekVisits: 0,
        monthVisits: 0,
        yearVisits: 0,
        schoolName: schoolAccount.schoolName,
      };
    });

    await analyticsCtrl.collectStudentsVisits(schoolStudents);

    students = students.concat(schoolStudents);
    schools.push(schoolAccount.schoolName);
  }

  logger.info(
    `teacher pulled student info, name: ${req.user.name}, email: ${req.user.username}`
  );

  return res.json({ students, schools });
};

// get students and students data for teacher
exports.deleteStudent = async function (req, res, next) {
  if (!req.user) {
    return next(new Error("Teacher is not logged in"));
  }

  if (req.user.profile !== "teacher" && req.user.profile !== "admin") {
    return next(new Error("User is not a teacher"));
  }
  try {
    await User.findOneAndDelete({ _id: req.query.id });
    logger.info(
      `student ${req.query.name}, access code ${req.query.accessCode}, was deleted by ${req.user.name}`
    );
  } catch (e) {
    return next(new Error(e));
  }

  return res.sendStatus(200);
};
