const moment = require("moment");
const UserData = require("../../models/UserData");
const SchoolAccount = require("../../models/SchoolAccount");

// update user visits by year { month { day }}}
exports.postUserVisits = async function (req, res, next) {
  if (!req.user) {
    return next(new Error("Student is not logged in"));
  }

  if (req.user.profile !== "student") {
    return next(new Error("User is not a teacher"));
  }

  const t = getCurrentTime();

  const field = "visits." + t.year + "." + t.month + "." + t.day;
  let update = { baseAccessCode: req.user.baseAccessCode, name: req.user.name };
  update[field] = 1;

  try {
    const doc = await UserData.updateOne(
      { userId: req.user.id },
      { $set: update },
      { upsert: true }
    );
  } catch (e) {
    res.status(500).send(e);
  }
  res.sendStatus(200);
};

exports.collectStudentsVisits = async function (studentAccounts) {
  let select = {};

  for (let [index, student] of studentAccounts.entries()) {
    const data = await UserData.findOne({ userId: student.id }).lean().exec();

    if (data) {
      const visits = getData(data.visits);
      studentAccounts[index].weekVisits = visits.week;
      studentAccounts[index].monthVisits = visits.month;
      studentAccounts[index].yearVisits = visits.week;
    }
  }
};

// get current time
function getCurrentTime() {
  moment.locale("he");

  const tTime = moment(),
    year = tTime.year(),
    month = tTime.month() + 1,
    date = tTime.date(),
    t = { day: date, month: month, year: year };

  return t;
}

// get visits for year month day
function getData(data) {
  let year = 0,
    month = 0,
    week = 0;

  for (let i = 365; i >= 0; --i) {
    let now = moment().subtract(i, "days"),
      y = now.year().toString(),
      m = (now.month() + 1).toString(),
      d = now.date().toString();

    let visits = 0;
    if (
      data.hasOwnProperty(y) &&
      data[y].hasOwnProperty(m) &&
      data[y][m].hasOwnProperty(d)
    ) {
      visits = data[y][m][d];
    }
    // year
    year += visits;

    // month
    if (i < 30) {
      month += visits;
    }

    // days
    if (i < 7) {
      week += visits;
    }
  }

  return { year, month, week };
}
