module.exports = {
  ForgotPassword: {
    subject: "שלום {{name}}, יצרנו לכם סיסמא חדשה עבור אתר האנתולוגיה",
    p1: { type: "html", inner: "<h3>שלום {{name}}</h3>" },
    p2: {
      type: "p",
      inner:
        "חידשנו לכם את סיסמאת הכניסה לאתר האנתולוגיה, הסיסמא החדשה היא {{password}}. הכנסו לאתר כעת עם המייל שלכם והסיסמא החדשה",
    },
  },
  TeacherCanceledHisPremium: {},
  TeacherHasFiveStudent: {
    subject: "A Premium Teacher has 5 student registered",
    p1: {
      type: "p",
      inner:
        "the teacher: {{name}} with email: {{email}} has five students connected",
    },
  },
};
