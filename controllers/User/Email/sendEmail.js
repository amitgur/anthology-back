const fs = require("fs"),
  juice = require("juice"),
  swig = require("swig"),
  sendGridHandler = require("../../../hendrix/sendGridHandler"),
  emailDatas = require("./emailData");

//send email to user
module.exports = function (name, email, emailTemplate, options, callback) {
  let emailData = emailDatas[emailTemplate],
    innerHtml = "",
    html;

  for (let htmlKey in emailData) {
    if (emailData[htmlKey].type === "p") {
      innerHtml +=
        "<p>" + swig.render(emailData[htmlKey].inner, options) + "</p>";
    }
    if (emailData[htmlKey].type === "html") {
      innerHtml += swig.render(emailData[htmlKey].inner, options);
    }
  }

  fs.readFile("./views/emails/anthologyEmail.css", function (err, emailCss) {
    if (err) {
      return callback(err);
    }

    swig.setDefaults({ autoescape: false });
    let emailHtml = swig.renderFile("./views/emails/anthologyEmail.html", {
      name: name,
      innerHtml: innerHtml,
    });

    html = juice.inlineContent(emailHtml, emailCss.toString());
    sendGridHandler
      .sendMail(swig.render(emailData.subject, options), html, email)
      .then(
        () => {
          return callback();
        },
        (err) => {
          return callback(err);
        }
      );
  });
};
