const User = require("../../models/User");
const userCtrl = require("../User/accessCodeCtrl");
const SchoolAccount = require("../../models/SchoolAccount");
const NotPaid = require("../../models/NotPaid");
const sendEmail = require("./Email/sendEmail");
const logger = require("../../hendrix/logger").logger;
const crypto = require("crypto");
const fetch = require("node-fetch");

// SignUp Post
exports.signUp = function (req, res, next) {
  if (!res.locals.recaptcha.success) {
    console.log(res.locals.recaptcha);
    return res.json({ status: "error", msg: "אירעה בעית אבטחה בעת ההרשמה" });
  }
  if (
    req.body.magicWord !== process.env.MAGIC_WORD &&
    req.body.magicWord !== "MusicIsFun"
  ) {
    const info = "username: " + req.body.username;
    return res.json({ status: "error", msg: "מילת הקסם אינה נכונה" });
  }

  delete req.body.magicWord;

  let user = new User(req.body);
  let message = null;

  user.save(function (err) {
    if (err) {
      console.log("Error in create user: " + err, "error");
      console.log(err);
      switch (err.code) {
        case 11000:
        case 11001:
          message = " מייל זה כבר קיים אצלנו ";
          break;
        default:
          message = "  אנא מלא את כל השדות ";
      }

      return res.json({ status: "error", msg: message });
    }

    return res.json({
      status: "ok",
      msg:
        "ההרשמה עברה בהצלחה. אנא פנו לרכז המגמה על מנת להסדיר את הרשאתכם לשימוש באתר.",
    });
  });
};

exports.profileEdit = async function (req, res, next) {
  const id = req.body.id;
  let doc;
  delete req.body.id;
  try {
    doc = await User.findOne({ _id: id });
  } catch (e) {
    console.error(e);
    res.status(500).send(e);
  }

  if (!doc) {
    console.error(`id not found in profileEdit: ${id}`);
    return res.status(500).send("id not found in profileEdit");
  }

  if (req.body.name) {
    doc.name = req.body.name;
  }

  if (req.body.password) {
    doc.password = req.body.password;
  }

  doc.save((err) => {
    if (err) {
      console.error(e);
      res.status(500).send(e);
    } else {
      return res.sendStatus(200);
    }
  });
};

exports.forgotPassword = async function (req, res, next) {
  const email = req.body.email;
  let doc;
  const newPassword = exports.random(4, "0123456789");

  try {
    doc = await User.findOne({ username: email });
  } catch (err) {
    console.error("Error in forgotPassword");
    console.error(err);
    return res.sendStatus(500);
  }

  // user not found
  if (!doc) {
    return res.sendStatus(200);
  }

  doc.set("password", newPassword);
  doc.save((err) => {
    if (err) {
      console.error("Error in forgotPassword");
      console.error(err);
      return res.sendStatus(500);
    }

    let options = {
      locals: {
        name: doc.name,
        server: process.env.SERVER,
        password: newPassword,
        email: email,
      },
    };

    sendEmail(doc.name, email, "ForgotPassword", options, function (err) {
      if (err) {
        console.error("Error in forgotPassword");
        console.error(err);
        return res.sendStatus(500);
      }
      logger.info(
        "Forgot Password, name: " + " email: " + email + " new: " + newPassword
      );
      res.sendStatus(200);
    });
  });
};

// SignUp Admin Post
exports.studentSignUp = async function (req, res, next) {
  // get the school
  try {
    const newSchoolAccount = await SchoolAccount.findOne({
      baseAccessCode: req.body.baseAccessCode,
    }).lean();

    // check errors
    if (!newSchoolAccount) {
      return next(new Error("School code wasn't found"));
    }
    if (!newSchoolAccount.lastIndex >= newSchoolAccount.studentsNumber) {
      return next(new Error("No more students left in this school"));
    }

    // create user
    req.body.password = "BandPad135";
    req.body.username = userCtrl.createStudentAccessCode(
      req.body.baseAccessCode,
      newSchoolAccount.lastIndex
    );
    let user = new User(req.body);
    let message = null;

    user.save(async function (err) {
      if (err) {
        logger.error(
          `Error in create access code: ${req.body.accessCode}, ${err}`
        );
        switch (err.code) {
          case 11000:
          case 11001:
            message = `Error this code: ${req.body.accessCode}, already exists `;
            break;
          default:
            message = "  Please fill all the required fields";
        }

        return next(
          new Error(
            `Error during creating new access code ${req.body.accessCode}: ${message}`
          )
        );
      }

      await SchoolAccount.findOneAndUpdate(
        {
          baseAccessCode: req.body.baseAccessCode,
        },
        { $inc: { lastIndex: 1 } }
      );
      req.logIn(user, function (err) {
        if (err) {
          return next(err);
        }
        return res.send(req.user.username);
      });
    });
  } catch (e) {
    return next(e);
  }
};

exports.signOut = async function (req, res) {
  await req.logOut();
  req.session.destroy(function (err) {
    return res.sendStatus(200);
  });
};

// get user after authenticate was approved
exports.getUser = async function (req, res) {
  if (!req.user) {
    return res.status(403).json({ message: "לא נמצא משתמש מחובר" });
  } else {
    let user = {
      id: req.user._id,
      name: req.user.name,
      profile: req.user.profile, //profile is set on client side
      schools: req.user.schools,
    };

    // check if user paid
    const notPaid = await checkNotPaid(req.user);
    if (notPaid) {
      let user = Object.assign(req.user);
      logger.info(
        `not paid is trying to log in, name: ${JSON.stringify(user)}`
      );

      return res.json({
        status: "fail",
        message: "חשבון זה הושהה עקב אי תשלום, אנא פנו לרכז המגמה",
      });
    }

    /*********
     * STUDENT
     */

    // add accessCode on students
    if (req.user.profile === "student") {
      const schoolAccount = await SchoolAccount.findOne(
        {
          baseAccessCode: req.user.baseAccessCode,
        },
        { yearPass: true, _id: false }
      );
      if (
        !schoolAccount.yearPass.includes(process.env.THIS_YEAR) &&
        process.env.THIS_YEAR_AUTHENTICATION === "on"
      ) {
        logger.info(
          `student with no year pass is trying to login, name: ${req.user.name}, accessCode: ${user.accessCode}`
        );

        req.flash(
          "error",
          "למגמה זו לא קיים חידוש שימוש לשנת הלימודים הנוכחית, אנא פנו לרכז המגמה שלכם"
        );

        return res.send(403);
      }
      user.accessCode = req.user.username;
      logger.info(
        `student logged in, name: ${req.user.name}, accessCode: ${user.accessCode}`
      );
      return res.json(user);
    }

    /*********
     * TEACHER
     */

    if (req.user.profile === "teacher") {
      // check if teacher has school accounts
      const findArr = await SchoolAccount.find({
        "teachers.id": user.id.toString(),
      });
      // teacher has more than one school account
      if (findArr.length >= 1) {
        // check of new year payment
        const newYear = findArr.filter((s) => {
          return s.yearPass && s.yearPass.includes(process.env.THIS_YEAR);
        });

        // check year pass for current year
        if (
          newYear.length === 0 &&
          process.env.THIS_YEAR_AUTHENTICATION === "on"
        ) {
          logger.info(
            `teacher with no year pass is trying to login, name: ${user.name}, email: ${req.user.username}`
          );

          return res.status(403).json({
            message:
              "בתי הספר שהנכם משויכים אליהם לא חידשו את הרשיון לאתר לשנה זו, אנא פנו לרכזי המגמה",
          });
        }
        logger.info(
          `teacher logged in, name: ${user.name}, email: ${req.user.username}`
        );
        return res.json(user);
      } else {
        // no school account founds for this teacher
        console.log("teacher with no account is trying to login");
        logger.info(
          `teacher with no account is trying to login, name: ${user.name}, email: ${req.user.username}`
        );

        return res.status(403).json({
          message:
            "לחשבון זה אין הרשאה לכניסה בשנת לימודים זו, אנא פנו לקהילה מנגנת כדי להסדיר את ההרשמה",
        });
      }
    }

    /***
     * ADMIN
     */

    if (req.user.profile === "admin") {
      logger.info(`admin logged in`);
      return res.json(user);
    }
  }
};

// get error message
exports.getMessage = function (req, res) {
  let msg = req.flash();
  if (msg.hasOwnProperty("error")) {
    msg = msg.error[0];
  }
  return res.send(msg);
};

exports.authenticateAdmin = function (req, res, next) {
  let err;

  if (!req.user) {
    return res.status(403).json({ message: "לא נמצא משתמש מחובר" });
  }
  req.session.cookie.maxAge = 1000 * 60 * 60 * 24 * 60; // 2 month session

  console.log("Authenticate administrator " + req.user.username);
  return next();
};

exports.getTeachers = async function (req, res, next) {
  try {
    const teachers = await User.find(
      { profile: "teacher" },
      { name: true, _id: true }
    ).exec();
    return res.json(teachers);
  } catch (e) {
    return next(e);
  }
};

exports.getAllTeacherFields = async function (req, res, next) {
  try {
    const teachers = await User.find(
      { profile: "teacher" },
      { salt: false, hashed_password: false, schools: false }
    )
      .lean()
      .exec();

    const schoolAccounts = await SchoolAccount.find({}).lean();

    // collect teachers from school accounts
    for (let teacher of teachers) {
      teacher.schools = [];
      for (let schoolAccount of schoolAccounts) {
        for (const t of schoolAccount.teachers) {
          if (t.id && t.id.localeCompare(teacher._id) === 0) {
            teacher.schools.push(schoolAccount.schoolName);
          }
        }
      }
    }

    return res.json(teachers);
  } catch (e) {
    return next(e);
  }
};

// create random string by available chars
// http://blog.tompawlak.org/how-to-generate-random-values-nodejs-javascript
exports.random = function (howMany, chars) {
  chars =
    chars || "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

  var rnd = crypto.randomBytes(howMany),
    value = new Array(howMany),
    len = chars.length;

  for (var i = 0; i < howMany; i++) {
    value[i] = chars[rnd[i] % len];
  }
  return value.join("");
};

exports.verifyCapacha = function (req, res, next) {
  const VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";

  console.log(`verify recapacha with token ${req.body.token}`);
  logger.info(`verify recapacha with token ${req.body.token}`);

  return fetch(VERIFY_URL, {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: `secret=${process.env.RECAPACHA_SECRET_KEY}&response=${req.body.token}`,
  })
    .then((response) => response.json())
    .then((data) => {
      res.locals.recaptcha = data;
      return next();
    })
    .catch((err) => {
      return next(err);
    });
};

async function checkNotPaid(user) {
  user = user.toObject();
  let notPaid = await NotPaid.find({}, { baseAccessCode: true, _id: false });
  notPaid = notPaid.map((doc) => {
    return doc.baseAccessCode;
  });
  if (user.profile === "student") {
    return notPaid.indexOf(user.baseAccessCode) > -1;
  }

  if (user.profile === "teacher") {
    let schools = await SchoolAccount.find(
      {},
      { baseAccessCode: true, teachers: true, _id: true }
    )
      .lean()
      .exec();

    let teachers = [];

    schools = schools.filter((doc) => {
      return !notPaid.includes(doc.baseAccessCode);
    });

    // allowed teachers
    for (const school of schools) {
      teachers = teachers.concat(school.teachers);
    }

    teachers = teachers.map((t) => t.name);
    return !teachers.includes(user.name);
  }
}
