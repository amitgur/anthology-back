const createArticle = require("./createAtricle");

exports.getArticle = async (req, res) => {
  const docId = req.query.docId;
  const article = await createArticle(docId);
  res.send(replaceAll(article, "./public/article-images/", "/article-images/"));
};

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, "g"), replace);
}
