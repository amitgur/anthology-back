const articleController = require("./articleCtrl");

module.exports = function (app) {
  app.get("/get_article", articleController.getArticle);
};
