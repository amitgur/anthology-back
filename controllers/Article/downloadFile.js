/**
 * Download a docx file located on the google drive. then you can change it to html using
 * pandoc file.docx -f docx -t html -o index.html
 * @type {string}
 */
const apiKey = "AIzaSyBxiSUkCzfc8iaX-cMFOo4HkVzRbfGJ2Wo";
const axios = require("axios");
const fs = require("fs");

module.exports = async function downloadDoc(fileName, fileId) {
  const url = `https://www.googleapis.com/drive/v3/files/${fileId}?key=${apiKey}&alt=media`;
  const writer = fs.createWriteStream(`./temp/${fileName}.docx`);

  const response = await axios({
    url,
    method: "GET",
    responseType: "stream",
  });
  response.data.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on("finish", resolve);
    writer.on("error", reject);
  });
};
