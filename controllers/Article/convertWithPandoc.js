const pandoc = require("node-pandoc");

module.exports = async function convertWithPandoc(folderName, fileName) {
  const args = [
    "-f",
    "docx",
    "-t",
    "html",
    "--extract-media",
    `./public/article-images/${folderName}`,
  ];
  return new Promise((resolve, reject) => {
    // Call pandoc
    pandoc(`./${folderName}/${fileName}.docx`, args, function (err, htmlFile) {
      // some errors can happened with files that already exist
      if (err) {
        console.log(err);
      } else {
        console.log("returning");
        console.log(htmlFile);
        resolve(htmlFile);
      }
    });
  });
};
