const downloadDoc = require("./downloadFile");
const convertWithPandoc = require("./convertWithPandoc");
const fs = require("fs");

const fileName = "tempDoc";
const folderName = `temp`;

// Set your callback function
module.exports = async function createArticle(fileId) {
  return new Promise(async (resolve, reject) => {
    try {
      await downloadDoc(fileName, fileId);
      const article = await convertWithPandoc(folderName, fileName);
      resolve(article);
    } catch (err) {
      console.error(err);
      reject(err);
    }
  });
};
