const schoolCtrl = require("./schoolCtrl");

module.exports = function (app) {
  app.post("/create_school", schoolCtrl.createSchool);
  app.get("/get_schools", schoolCtrl.getSchools);
};
