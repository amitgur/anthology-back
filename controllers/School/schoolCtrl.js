const School = require("../../models/School");

exports.createSchool = async function (req, res, next) {
  try {
    const school = new School(req.body);
    let newSchool = await school.save();
    return res.sendStatus(200);
  } catch (e) {
    return next(e);
  }
};

// get all the cities for step 1
exports.getSchools = async function (req, res, next) {
  try {
    const schools = await School.find({}).exec();
    return res.json(schools);
  } catch (e) {
    return next(e);
  }
};
